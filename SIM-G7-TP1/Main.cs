﻿using SIM_G7_TP1.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SIM_G7_TP1
{
    public partial class Main : Form
    {
        double[] randomNumbers;

        public Main()
        {
            InitializeComponent();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            switch (tabRandomMethods.SelectedIndex)
            {
                case 0:
                    generateCongrLineal();
                    break;
                case 1:
                    generateCongrMulti();
                    break;
                case 2: 
                    generateLanguageRandom();
                    break;
                default:
                    break;
            }
        }

        private void generateLanguageRandom()
        {
            int rndNumCount = Convert.ToInt16(nudRandomNumbersCount.Value);
            int seed = Convert.ToInt16(nudLangSeed.Value);
            RandomGenerator rndGen = new RandomGenerator(seed);

            randomNumbers = rndGen.generateLangRandom(rndNumCount);
            fillDGNumbers(randomNumbers);
            generateFrecuencies();
        }

        private void generateCongrLineal()
        {
            int rndNumCount = Convert.ToInt16(nudRandomNumbersCount.Value);
            int seed = Convert.ToInt16(nudCongrLinealSemilla.Value);
            int constMulti = Convert.ToInt16(nudCongrLinealConstMulti.Value);
            int constAditiv = Convert.ToInt16(nudCongrLinealConstAditiva.Value);
            int magMod = Convert.ToInt16(nudCongrLinealMagnitudModulo.Value);

            RandomGenerator rndGen = new RandomGenerator(seed, constMulti, magMod, constAditiv);

            randomNumbers = rndGen.generateCongrLinealRandom(rndNumCount);
            fillDGNumbers(randomNumbers);
            generateFrecuencies();
        }

        private void generateCongrMulti()
        {
            int rndNumCount = Convert.ToInt16(nudRandomNumbersCount.Value);
            int seed = Convert.ToInt16(nudCongrMultiSeed.Value);
            int constMulti = Convert.ToInt16(nudCongrMultiConstMulti.Value);
            int magMod = Convert.ToInt16(nudCongrMultiMagnitudModulo.Value);

            RandomGenerator rndGen = new RandomGenerator(seed, constMulti, magMod, 0);

            randomNumbers = rndGen.generateCongrMultiRandom(rndNumCount);
            fillDGNumbers(randomNumbers);
            generateFrecuencies();
        }

        private void generateFrecuencies()
        {
            RandomGenerator gen = new RandomGenerator();
            int numIntervals = Convert.ToInt16(nudNumInvervals.Value);

            double[,] frecuencias = gen.validateFrecuencies(randomNumbers, numIntervals);

            fillDBFrecuencies(frecuencias);
            fillChart(frecuencias);
        }

        private void fillDGNumbers(double[] num)
        {
            dtgNumeros.Rows.Clear();
            for (int i = 0; i < num.Length; i++)
            {
                dtgNumeros.Rows.Add(i + 1, num[i]);
            }
        }

        private void fillDBFrecuencies(double[,] frec)
        {
            dtgIntervalos.Rows.Clear();
            String intervalo = "";

            for (int i = 0; i < frec.GetLength(0); i++)
            {
                intervalo = frec[i, 0] + " - " + frec[i, 1];
                dtgIntervalos.Rows.Add(intervalo, frec[i, 2], frec[i, 3], frec[i, 4], frec[i, 5]);
            }

        }

        private void fillChart(double[,] frec)
        {
            String intervalo;

            chtFrecGraph.Titles.Clear();
            chtFrecGraph.Series.Clear();

            chtFrecGraph.Titles.Add("Frecuencias");
            chtFrecGraph.Series.Add("Observadas");



            for (int f = 0; f < frec.GetLength(0); f++)
            {
                intervalo = frec[f, 0] + " - " + frec[f, 1];
                chtFrecGraph.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                chtFrecGraph.Series[0].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;
                chtFrecGraph.Series[0].XAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;
                chtFrecGraph.Series[0].Points.AddXY(intervalo, frec[f, 2]);
                //chtFrecGraph.

                graficoObtenida.Titles["grafico1"].Text = "Frecuencia Obtenida / Esperada";

                intervalo = frec[f, 0] + " - " + frec[f, 1];
                graficoObtenida.Series["Observada"].Points.AddXY(frec[f, 0], frec[f, 1]);
                graficoObtenida.Series["Observada"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                graficoObtenida.Series["Observada"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;
                graficoObtenida.Series["Observada"].XAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;
                graficoObtenida.Series["Esperada"].Points.AddXY(intervalo, frec[f, 2]);
                graficoObtenida.Series["Esperada"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                graficoObtenida.Series["Esperada"].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;
                graficoObtenida.Series["Esperada"].XAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;
                graficoObtenida.Hide();
            }

            chtFrecGraph.ChartAreas[0].AxisX.MajorGrid.Enabled = false;


        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dtgNumeros.Rows.Clear();
        }
    }
}
